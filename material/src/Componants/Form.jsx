import React, { useState } from "react";
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  FormGroup,
  FormControlLabel,
  Checkbox,
  FormLabel,
  RadioGroup,
  Radio,
} from "@mui/material";

const MyForm = () => {
  const [input, setInput] = useState({
    name: "",
    email: "",
    password: "",
    couses: "",
    turm: false,
    gender: "",
  });

  const handleChange = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };

  const handleSelectChange = (e) => {
    setInput({
      ...input,
      couses: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(input);

    // Clear the form after submission
    setInput({
      name: "",
      email: "",
      password: "",
      couses: "",
    });
  };

  return (
    <>
      <TextField
        name="name"
        value={input.name}
        onChange={handleChange}
        placeholder="Enter your name"
        sx={{ marginLeft: "40%", marginTop: "10px" }}
      />
      <br />
      <TextField
        name="email"
        value={input.email}
        onChange={handleChange}
        placeholder="Enter your email"
        sx={{ marginLeft: "40%", marginTop: "10px" }}
      />
      <br />
      <TextField
        name="password"
        value={input.password}
        onChange={handleChange}
        placeholder="Enter your password"
        sx={{ marginLeft: "40%", marginTop: "10px" }}
      />
      <br />
      <FormControl fullWidth sx={{ marginLeft: "40%", marginTop: "10px" }}>
        <InputLabel id="menu" sx={{ width: "200px" }}>
          Courses
        </InputLabel>
        <Select
          onChange={handleSelectChange}
          labelId="menu"
          sx={{ width: "230px" }}
          label="Courses"
          name="couses"
          value={input.couses}
        >
          <MenuItem value={"mongodb"}>mongodb</MenuItem>
          <MenuItem value={"react"}>react</MenuItem>
          <MenuItem value={"node"}>node</MenuItem>
          <MenuItem value={"react native"}>react native</MenuItem>
        </Select>
      </FormControl>
      <br />
      <FormGroup>
        <FormControlLabel
          label="i agreee T&T"
          sx={{ marginLeft: "40%", marginTop: "10px" }}
          control={
            <Checkbox
              onChange={() =>
                setInput((preState) => ({
                  ...preState,
                  turm: !input.turm,
                }))
              }
            />
          }
        />
      </FormGroup>
      <br />
      <FormControl sx={{ marginLeft: "40%", marginTop: "10px" }}>
        <FormLabel>gender</FormLabel>
        <RadioGroup name="gender" defaultValue={"male"} onChange={handleChange}>
          <FormControlLabel value={"male"} label="male" control={<Radio />} />
          <FormControlLabel value={"female"} label="female" control={<Radio />}/>
          <FormControlLabel value={"othees"} label="others" control={<Radio />} />
        </RadioGroup>
      </FormControl>
      <br />
      <Button
        variant="contained"
        sx={{ marginLeft: "40%", marginTop: "10px" }}
        onClick={handleSubmit}
      >
        Click me
      </Button>
    </>
  );
};

export default MyForm;
