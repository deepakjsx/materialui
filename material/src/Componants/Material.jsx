import React from 'react'
import {Typography,Button,TextField} from "@mui/material"
const Material = () => {
  return (
    <div>
      <h1>Material</h1>
      <Typography variant="h4" sx={{ color: "red" }}>
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
        ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
        dis parturient montes, nascetur ridiculus mus. Donec quam felis,
        ultricies nec, pellentesque eu, pretium quis, sem.
      </Typography>
      <Button variant="contained" color="success">
        summit
      </Button>
      <Button variant="text" size="large">
        summit
      </Button>
      <Button
        variant="outlined"
        sx={{ margin: "50px" }}
        onClick={() => alert("actions")}
      >
        summit
      </Button>
      <br />
      <TextField
        variant="standard"
        type="text"
        placeholder="name"
        sx={{ width: "300px" }}
      >
        deepak yadav
      </TextField>
      <br />
      <TextField
        variant="outlined"
        type="text"
        placeholder="name"
        sx={{ width: "300px",marginTop:"10px" }}
      >
        deepak yadav
          </TextField>
          <br/>
      <TextField
        variant="filled"
        type="text"
        placeholder="name"
        sx={{ width: "300px",marginTop:"10px" }}
      >
        deepak yadav
      </TextField>
    </div>
  );
}

export default Material
